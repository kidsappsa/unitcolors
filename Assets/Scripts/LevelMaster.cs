﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelMaster : MonoBehaviour {
	public ColoredCellsGrid grid;

	public ColorPickerPanel ColorsPanel;

	private int currentProcess;
	public event System.Action WonEvent;
	public event System.Action ClickEvent;

	// Use this for initialization
	void Awake () {
		grid.InstantiateCallBack += delegate(Cell cell) {
			(cell as ColoredCell).ClickedEvent += delegate(Cell __cell) {
				Clicked(__cell);	
			};
		};
	}

	public void Reset(){
		grid.Clear ();
		grid.PopulateGrid (true);
	}
		
	void Clicked(Cell clickedCell){
		StartCoroutine (ChangeColorSpace(clickedCell));
	}

	//TODO: Create ColorRegion Class
	IEnumerator ChangeColorSpace(Cell clickedCell){
		Color originalCellColor = (clickedCell as ColoredCell).Color;
		if (originalCellColor == GetClickColor ())
			yield break;

		if (ClickEvent != null)
			ClickEvent ();
		
		currentProcess++;

		Color clickColor = GetClickColor ();

		List<Cell> OpenList = new List<Cell>();
		OpenList.Add (clickedCell);

		while (OpenList.Count > 0 ) {
			List<Cell> TempList = new List<Cell>();
			foreach (Cell _cell in OpenList) {
				(_cell as ColoredCell).Color = clickColor;

				foreach (var item in grid.GetDirectNeighbors(_cell.coord)) {
					if((item as ColoredCell).Color == originalCellColor){
						TempList.Add (item);
					}
				}
			}
			OpenList.Clear ();
			OpenList.AddRange (TempList);
			yield return new WaitForSeconds (0.1f);
		}
		currentProcess--;
		CheckForWin ();
	}

	Color GetClickColor(){
		return ColorsPanel.GetCurrentColor ();
	}

	void CheckForWin(){
		if (currentProcess > 0)
			return;

		if (grid.AllIsOneColor ())
			Win ();
	}

	void Win(){
		if (WonEvent != null)
			WonEvent ();
	}

	public void SetLevel(Level level){
		grid.level = level;
	}
}
