﻿using UnityEngine;
using System.Collections;

public static class Colors {
	public static Color RED = Color.red;
	public static Color GREEN = Color.green;
	public static Color BLUE = Color.blue;

	public static Color GetRandom(){
		return Random.value > 0.5f ? RED : ( Random.value > 0.5f ? GREEN : BLUE); 
	}

	public enum Named{
		Red,
		Green,
		Blue
	}

	public static Color GetColor(Named _color){
		switch (_color) {
		case Named.Blue:
			return BLUE;
		case Named.Green:
			return GREEN;
		case Named.Red:
			return RED;
		default:
			return BLUE;;
		}

	}
}
