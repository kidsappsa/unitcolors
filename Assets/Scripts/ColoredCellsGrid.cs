﻿using UnityEngine;
using System.Collections.Generic;

public class ColoredCellsGrid : CellsGrid {
	public bool isSmoothMap;
	public bool ExtraProcess;
	public int SmoothFactor = 5;

	public Level level;

	public override void Awake(){
		base.Awake();
		InstantiateCallBack += delegate(Cell cell) {
			(cell as ColoredCell).Color = GetInitCellColor(cell.coord);
		};
	}

	Color GetInitCellColor(Coord coord){
		if (level == null)
			return Colors.GetRandom ();
		else {
			return level.data[(coord.x * map.size.y) + coord.y].color;
		}
	}

	public void PopulateGrid(bool PostProcess){
		PopulateGrid();

		if (!PostProcess || level != null)
			return;
		
		if (isSmoothMap) {
			for (int i = 0; i < SmoothFactor; i++) {
				SmoothMap ();
			}
		}
		if (ExtraProcess)
			ProcessMap ();
	}

	void ProcessMap(){
		foreach (var item in new Colors.Named[]{Colors.Named.Blue,Colors.Named.Green,Colors.Named.Red}) {
			List<List<Coord>> wallRegions = GetRegions (Colors.GetColor(item));
			int wallThresholdSize = 3;

			foreach (List<Coord> wallRegion in wallRegions) {
				if (wallRegion.Count < wallThresholdSize) {
					foreach (Coord tile in wallRegion) {
						var val = GetDominantColor (GetCell (tile));
						(GetCell(tile) as ColoredCell).Color = val.Key;
					}
				}
			}
		}
	}

	void SmoothMap(){
		for (int x = 0; x < map.size.x; x ++) {
			for (int y = 0; y < map.size.y; y ++) {
				Cell cell = Cells [x, y];

				var val = GetDominantColor (cell);

				if(val.Value > 2)
					(cell as ColoredCell).Color = val.Key;
			}
		}
	}

	public bool AllIsOneColor(){
		Color firstCellColor = (Cells[0,0] as ColoredCell).Color;
		for (int x = 0; x < map.size.x; x ++) {
			for (int y = 0; y < map.size.y; y ++) {
				Cell cell = Cells [x, y];
				if( (cell as ColoredCell).Color != firstCellColor) return false;
			}
		}
		return true;
	}

	KeyValuePair<Color, int> GetDominantColor(Cell cell){
		List<Cell> neighbours = GetDirectNeighbors(cell.coord);

		Dictionary<Color, int> dictionary =
			new Dictionary<Color, int>();

		foreach (var item in neighbours) {
			Color itemColor = (item as ColoredCell).Color;
			if (dictionary.ContainsKey (itemColor)) {
				dictionary[itemColor] =  ++dictionary[itemColor];
			} else {
				dictionary.Add (itemColor, 1);
			}
		}

		Color DominantColor = (cell as ColoredCell).Color;
		int counter = 0;
		foreach (var item in dictionary) {
			if (item.Value > counter) {
				counter = item.Value;
				DominantColor = item.Key;
			}
		}

		return new KeyValuePair<Color, int> (DominantColor, counter);
	}

	List<List<Coord>> GetRegions(Color tileType) {
		List<List<Coord>> regions = new List<List<Coord>> ();
		int[,] mapFlags = new int[map.size.x, map.size.y];

		for (int x = 0; x < map.size.x; x ++) {
			for (int y = 0; y < map.size.y; y ++) {
				if (mapFlags[x,y] == 0 && (GetCell(new Coord(x,y)) as ColoredCell).Color == tileType) {
					List<Coord> newRegion = GetRegionTiles(x,y);
					regions.Add(newRegion);

					foreach (Coord tile in newRegion) {
						mapFlags[tile.x, tile.y] = 1;
					}
				}
			}
		}

		return regions;
	}

	List<Coord> GetRegionTiles(int startX, int startY) {
		List<Coord> tiles = new List<Coord> ();
		int[,] mapFlags = new int[map.size.x,map.size.y];
		Color tileType = (GetCell(new Coord(startX, startY)) as ColoredCell).Color;

		Queue<Coord> queue = new Queue<Coord> ();
		queue.Enqueue (new Coord (startX, startY));
		mapFlags [startX, startY] = 1;

		while (queue.Count > 0) {
			Coord tile = queue.Dequeue();
			tiles.Add(tile);

			for (int x = tile.x - 1; x <= tile.x + 1; x++) {
				for (int y = tile.y - 1; y <= tile.y + 1; y++) {
					if (Contains(new Coord(x,y)) && (y == tile.y || x == tile.x)) {
						if (mapFlags[x,y] == 0 && (GetCell(new Coord(x,y)) as ColoredCell).Color == tileType) {
							mapFlags[x,y] = 1;
							queue.Enqueue(new Coord(x,y));
						}
					}
				}
			}
		}

		return tiles;
	}
}
