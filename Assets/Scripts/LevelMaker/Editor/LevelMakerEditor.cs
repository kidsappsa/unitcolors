﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(LevelMaker))]
public class LevelMakerEditor : GridMaker.LevelMakerEditor<ColoredCell, CellData> {
	protected override void SaveLevel ()
	{
		AssetDatabase.CreateAsset(levelMaker.SaveLevel(ScriptableObject.CreateInstance<Level> ()), "Assets/Levels/level.asset");
		AssetDatabase.SaveAssets();
	}
}
