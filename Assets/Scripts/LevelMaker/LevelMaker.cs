﻿using UnityEngine;
using System.Collections;

public class LevelMaker : GridMaker.LevelMaker<ColoredCell, CellData> {
	public GameObject prefap;

	public Colors.Named color;

	protected override CellData GetBrushData ()
	{
		CellData data = new CellData();
		data.color = Colors.GetColor (color);
		return data;
	}

	protected override GameObject PrepareCell (CellData data)
	{
		GameObject ob = Instantiate (prefap);
		ob.GetComponent<ColoredCell> ().Color = data.color;
		return ob;
	}
}
