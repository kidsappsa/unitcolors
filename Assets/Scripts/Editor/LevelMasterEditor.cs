﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(LevelMaster))]
public class LevelMasterEditor : Editor {

	public override void OnInspectorGUI ()
	{
		DrawDefaultInspector();

		if(GUILayout.Button("Reset")){
			(target as LevelMaster).Reset ();
		}
	}
}
