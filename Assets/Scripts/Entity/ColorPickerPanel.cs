﻿using UnityEngine;
using System.Collections;

public class ColorPickerPanel : MonoBehaviour {
	public Colors.Named[] ColorsList;
	public GameObject ColorPickerPrefap;

	public event System.Action<Color> ColorChangeEvent;
	private Color CurrentColor;
	private ColorPicker[] ColorPickers;

	void Awake () {
		ColorPickers = new ColorPicker[ColorsList.Length];
		for (int i = 0; i < ColorsList.Length; i++) {
			Colors.Named color = ColorsList [i];	
			GameObject g_object =  Instantiate (ColorPickerPrefap);
			g_object.transform.parent = transform;
			g_object.transform.localPosition = new Vector3 ( i - ColorsList.Length /2,0f,0f);
			ColorPicker picker = g_object.GetComponent<ColorPicker> ();
			picker.ClickedEvent+= delegate(ColorPicker colorPicker) {
				SetColorTo(colorPicker);
			};
			picker.Color = Colors.GetColor(color);
			ColorPickers [i] = picker;
		}
		SetColorTo (ColorPickers[0]);
	}

	void SetColorTo(ColorPicker colorPicker){
		if (CurrentColor == colorPicker.Color)
			return;

		CurrentColor = colorPicker.Color;

		foreach (var item in ColorPickers) {
			item.gameObject.transform.localScale = new Vector3 (2,2,2);
		}

		colorPicker.gameObject.transform.localScale = new Vector3 (2f,2.5f,1);
	}

	public Color GetCurrentColor(){
		return CurrentColor;
	}
}
