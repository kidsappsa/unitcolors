﻿using UnityEngine;
using System.Collections;

public class ColorPicker : MonoBehaviour {
	private SpriteRenderer spR;
	private Color color;
	public Color Color{
		get{
			return color;
		}
		set{
			color = value;
			spR.color = color;
		}
	}

	public event System.Action<ColorPicker> ClickedEvent;

	// Use this for initialization
	void Awake () {
		spR = GetComponent<SpriteRenderer> ();
		GetComponent<Clickable>().ActionEvent += delegate {
			if (ClickedEvent != null)
				ClickedEvent(this);
		};
	}
}
