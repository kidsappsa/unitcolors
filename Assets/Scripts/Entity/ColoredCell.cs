﻿using UnityEngine;
using System.Collections;

public class ColoredCell : Cell {
	private Color _color;
	public Color Color{
		get{
			return _color;
		}
		set{
			_color = value;
			spR.color = _color;
		}
	}

	public event System.Action<Cell> ClickedEvent;

	public SpriteRenderer spR;
	private Clickable clickable;

	void Awake () {
		GetComponent<Clickable>().ActionEvent += delegate {
			if (ClickedEvent != null)
				ClickedEvent(this);
		};
	}
}
