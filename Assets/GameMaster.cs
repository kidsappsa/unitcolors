﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameMaster : MonoBehaviour {
	public Clickable ResetBtn;
	public Text StepsCount;
	private int StepsCounter;
	public Level[] levels;

	private LevelMaster level;
	private int currentLevelIndex;


	// Use this for initialization
	void Awake () {
		level = GetComponent<LevelMaster> ();
		level.WonEvent += delegate {
			NextLevel();
		};
		level.ClickEvent += delegate {
			SetStepsCounter(StepsCounter + 1);
		};
		ResetBtn.ActionEvent += delegate {
			level.Reset();
			SetStepsCounter (0);
		};
	}

	void Start(){
		NextLevel ();
	}

	void NextLevel(){
		if (currentLevelIndex < levels.Length) {
			level.SetLevel (levels [currentLevelIndex++]);
		} else {
			level.SetLevel (null);
		}
		level.Reset ();
		SetStepsCounter (0);
	}

	void SetStepsCounter(int steps){
		StepsCounter = steps;
		StepsCount.text = "" + steps;
	}
}
